#include "httpd.h"

#define SIZE 10000000

char image[5][SIZE];
int length[5];
char slideshow[512];
int html_len;

int main(int c, char** v)
{
	FILE* fp = fopen("slideshow.html", "r");
	html_len = fread(slideshow, sizeof(char), 512, fp);
	fprintf(stderr, "Loaded html %d bytes\n", html_len);
	fclose(fp); 
	char file_name[20] = "image_";
	char temp[10];

	for (int i = 0; i < 5; i++) {
		file_name[6] = 0;
		sprintf(temp, "%d", i);
		strcat(file_name, temp);
		strcat(file_name, ".jpeg");
		fprintf(stderr, "Loading %s\n", file_name);
		fp = fopen(file_name, "r");
		length[i] = fread(image[i], sizeof(char), SIZE, fp);
		fclose(fp);
	}
    	serve_forever("80");
	return 0;
}

void route(int next)
{
    	ROUTE_START()

    	ROUTE_GET("/")
    	{
        	printf("HTTP/1.1 200 OK\n");
		printf("content-type: image/jpeg\n");
		printf("content-length: %d\n\n", length[next]);

		fwrite(image[next], sizeof(char), length[next], stdout);
    	}

	ROUTE_GET("/slideshow")
	{
		printf("HTTP/1.1 200 OK\n");
		printf("content-type: text/html\n");
		printf("content-length: %d\n\n", html_len);

		fwrite(slideshow, sizeof(char), html_len, stdout);
	}
 
    	ROUTE_END()
}

