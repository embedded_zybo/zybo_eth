version = "1.29"

# tools
dblatex = ''
fop = ''
highlight = ''
highlight_options = ''
pkg_config = '/home/zeljko/embedded_3/zybo_peta/build/tmp/work/plnx_zynq7-xilinx-linux-gnueabi/linux-xlnx/4.19-xilinx-v2019.1+gitAUTOINC+9811303824-r0/recipe-sysroot-native/usr/bin/pkg-config'
xsltproc = '/scratch/petalinux-yocto/yocto_downloads_2019.1_zynq-generic/build_zynq-generic/tmp/sysroots-xsct/Scout/2019.1/bin/xsltproc'

# configured directories
prefix = '/home/zeljko/embedded_3/zybo_peta/build/tmp/work/plnx_zynq7-xilinx-linux-gnueabi/linux-xlnx/4.19-xilinx-v2019.1+gitAUTOINC+9811303824-r0/recipe-sysroot-native/usr'
datarootdir = "${prefix}/share".replace('${prefix}', prefix)
datadir = "/home/zeljko/embedded_3/zybo_peta/build/tmp/work/plnx_zynq7-xilinx-linux-gnueabi/linux-xlnx/4.19-xilinx-v2019.1+gitAUTOINC+9811303824-r0/recipe-sysroot-native/usr/share".replace('${datarootdir}', datarootdir)

exeext = ''
