version = "1.29"

# tools
dblatex = ''
fop = ''
highlight = ''
highlight_options = ''
pkg_config = '/home/zeljko/embedded_3/zybo_peta/build/tmp/work/x86_64-linux/qemu-helper-native/1.0-r1/recipe-sysroot-native/usr/bin/pkg-config'
xsltproc = '/scratch/petalinux-yocto/yocto_downloads_2019.1_zynq-generic/build_zynq-generic/tmp/sysroots-xsct/Scout/2019.1/bin/xsltproc'

# configured directories
prefix = '/home/zeljko/embedded_3/zybo_peta/build/tmp/work/x86_64-linux/qemu-helper-native/1.0-r1/recipe-sysroot-native/usr'
datarootdir = "${prefix}/share".replace('${prefix}', prefix)
datadir = "/home/zeljko/embedded_3/zybo_peta/build/tmp/work/x86_64-linux/qemu-helper-native/1.0-r1/recipe-sysroot-native/usr/share".replace('${datarootdir}', datarootdir)

exeext = ''
